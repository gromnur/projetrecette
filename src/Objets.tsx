export type Recipe = {
    id: Number
    strMealThumb: string;
    nom: string;
    categorie: string;
    ingredients: Array<Ingredient>
    instruction: string;
    tags: String;
}

export type RootStackParamList = {
    Home: undefined;
    Search: undefined;
    Details: { recipe: Recipe } ;
};

export type Ingredient = {
    nom: string;
    qte: string;
}
