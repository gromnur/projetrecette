import React from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import {StackNavigationProp} from "@react-navigation/stack";
import {Recipe, RootStackParamList} from "../Objets";
import axios from "axios";
import {transformJsonToRecipe} from "../utils";

type Props = {
    navigation: StackNavigationProp<RootStackParamList>
}

const Home: React.FC<Props> = (props) => {

    const pressSearch = () => {
        props.navigation.navigate("Search");
    }

    const pressAlea = async () => {
        const res = await axios.get(`https://www.themealdb.com/api/json/v1/1/random.php`)
        const recipe: Recipe = transformJsonToRecipe(res)[0]
        props.navigation.navigate("Details", {recipe});
    }

    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.button} onPress={pressSearch}>
                <Text>Recherche</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.button} onPress={pressAlea}>
                <Text>Random</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        display: "flex",
        flexDirection: "row",
        backgroundColor: '#fff',
    },
    button: {
        flex: 1,
        alignItems: "center",
    },
});

export default Home
