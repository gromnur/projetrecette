import axios from 'axios';
import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { FlatList, SafeAreaView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import EmptyListPlaceholder from '../components/EmptyListPlaceholder';
import {transformJsonToRecipe} from "../utils";
import {StackNavigationProp} from "@react-navigation/stack";
import {Recipe, RootStackParamList} from "../Objets";
import RecipeCard from "../components/RecipeCard";


type Props = {
    navigation: StackNavigationProp<RootStackParamList>
}

const Search: React.FC<Props> = (props) => {

    const [query, setquery] = useState<string>('')
    const [recipes, setrecipes] = useState<any[]>([])

    const onPressSearch = async () => {
        if (query === '') {
            setrecipes([])
            console.log("Aucune recherche saisie")
            return
        }

        const res = await axios.get(`https://www.themealdb.com/api/json/v1/1/search.php?f=${query}`)

        console.log(res);
        if (res.data !== "") {
            let a = transformJsonToRecipe(res);
            console.log(a);
            setrecipes(a);
        } else {
            console.log("Aucune recette")
        }
    }

    return (
        <View style={styles.container}>
            <SafeAreaView>
                <Text style={styles.title}>Search a Recipe</Text>
                <TextInput value={query} onChangeText={setquery} style={styles.searchInput} />
                <TouchableOpacity style={styles.searchButton} onPress={onPressSearch}>
                    <Text style={styles.searchButtonText}>SEARCH</Text>
                </TouchableOpacity>
                <FlatList
                    data={recipes}
                    ListEmptyComponent={<EmptyListPlaceholder />}
                    keyExtractor={item => item.id}
                    renderItem={({item, index}) =>
                        { return <RecipeCard nav={props.navigation} recipe={item}></RecipeCard> }
                    }
                />
            </SafeAreaView>
        </View>
    );
}

const styles = StyleSheet.create({
    title: {
        fontWeight: '800',
        fontSize: 42,
        padding: 16,
    },
    searchInput: {
        fontWeight: '500',
        fontSize: 24,
        padding: 16,
        backgroundColor: '#F6F6F6'
    },
    searchButton: {
        padding: 16,
        display: "flex",
        alignItems: "center",
        justifyContent: 'center',
        backgroundColor: '#4630EB'
    },
    searchButtonText: {
        fontWeight: '600',
        fontSize: 24,
        color: 'white',
        letterSpacing: 4
    },
    list: {
        padding: 16,
        display: 'flex'
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    searchResult: {
        marginVertical: 10,
    }
});

export default Search
