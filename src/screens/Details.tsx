import React from 'react';
import {StyleSheet} from 'react-native';
import {StackNavigationProp} from "@react-navigation/stack";
import {RootStackParamList} from "../Objets";
import RecipeDetail from "../components/RecipeDetail";
import { RouteProp } from '@react-navigation/native';

type ProfileScreenRouteProp = RouteProp<RootStackParamList, 'Details'>;

type ProfileScreenNavigationProp = StackNavigationProp<
    RootStackParamList,
    'Details'
    >;

type Props = {
    route: ProfileScreenRouteProp;
    navigation: ProfileScreenNavigationProp;
};

const Details: React.FC<Props> = (props) => {

    const getDetail = () => {
        const {recipe} = props.route.params
        console.log(recipe)
        return recipe;
    }

    return (
        <RecipeDetail recipe={getDetail()}></RecipeDetail>
    )
}
const styles = StyleSheet.create({

});

export default Details
