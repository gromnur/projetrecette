import {Ingredient, Recipe} from "./Objets";

export function transformJsonToRecipe(json: any) : Array<Recipe> {

    return json.data.meals.map((item: any) => {
         let recipe: Recipe = {
            id: item.idMeal,
            nom: item.strMeal,
            categorie: item.strCategory,
            instruction: item.strInstructions,
            strMealThumb: item.strMealThumb,
            tags: item.strTags,
            ingredients: getIngredient(item)
        }

        return recipe;
    });
};

export function getIngredient(item: any) : Array<Ingredient> {
    const ingredients: Array<Ingredient> = [];
    let qte: Array<string> = [];
    let nom: Array<string> = [];
    Object.keys(item).map((key) => {
        if (key.includes("strIngredient")) {
            if (item[key] != null && item[key] != undefined){
                if (item[key].trim() !== "")
                    nom.push(item[key].toString());
            }
        }

        if (key.includes("strMeasure")) {
            if (item[key] != null && item[key] != undefined) {
                if (item[key].trim() !== "") {
                    qte.push(item[key].toString());
                }
            }
        }
    })

    for (let i = 0 ; i < qte.length ; i++) {
        ingredients.push({nom: nom[i],qte: qte[i]})
    }

    return ingredients;
};
