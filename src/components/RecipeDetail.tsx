import React, {Key} from 'react'
import {Image, StyleSheet, Text, View } from 'react-native';
import {Recipe} from "../Objets";

type Props = {
    recipe: Recipe | undefined
}

const RecipeDetail: React.FC<Props> = ({ recipe }) => {
    return (
        <View style={styles.container}>
            <View style={styles.imageContainer}>
                <Image source={{ uri: recipe?.strMealThumb }} style={styles.image} />
            </View>
            <View style={styles.infosContainer}>
                <Text style={styles.placeholder}>{recipe?.nom}</Text>
                <Text style={styles.placeholder}>{recipe?.categorie}</Text>
                <Text style={styles.placeholder}>{recipe?.instruction}</Text>
                <Text style={styles.placeholder}>Ingredient :</Text>
                {recipe?.ingredients.map((ingred) => {
                    return <View style={styles.ingredientsContainer}>
                        <Text style={styles.ingredientPlaceholder}>- {ingred.nom} : {ingred.qte}</Text>
                    </View>
                })}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: 400,
        marginVertical: 10,
        display: "flex",
        flexDirection: "column",
        borderWidth: 3,
        borderColor: "#c8c7c7",
        borderRadius: 50,
        alignItems: 'center',
        alignSelf: "center"
    },

    image: {
        borderRadius: 50,
        height: 400,
        width: 400,
        aspectRatio: 1,
    },
    infosContainer: {
        flex: 1,
        marginHorizontal: 50
    },
    ingredientsContainer: {
        display: "flex",
        flexDirection: "row",
        marginVertical: 2,
        paddingLeft: 5
    },
    title: {
        fontSize: 20,
        color: "#1f1f1f",
        marginBottom:10,
        fontWeight: "bold"
    },
    categorie: {
        color: "#1f1f1f",
        marginBottom:10,
        fontSize: 20
    },
    ingredientPlaceholder: {
        fontSize: 15,
        color: "#1f1f1f",
        marginBottom: 3
    },
    imageContainer: {
        width: 400,
        display: "flex",
        justifyContent: "center",
    },

    placeholder: {
        fontSize: 15,
        color: "#1f1f1f",
        marginBottom:10
    }
});

export default RecipeDetail
