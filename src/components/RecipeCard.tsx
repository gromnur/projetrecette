import React from 'react'
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Recipe, RootStackParamList} from "../Objets";
import {StackNavigationProp} from "@react-navigation/stack";

type Props = {
    recipe: Recipe,
    nav: StackNavigationProp<RootStackParamList>
}

const RecipeCard: React.FC<Props> = ({ recipe, nav }) => {

    const onPress = () => {
        nav.navigate("Details", {recipe})
    }

    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.imageContainer} onPress={onPress}>
                <Image source={{ uri: recipe.strMealThumb }} style={styles.image} />
            </TouchableOpacity>
            <View style={styles.infosContainer}>
                <Text style={styles.title}>{recipe.nom}</Text>
                <Text style={styles.categorie}>{recipe.categorie}</Text>
                <Text style={styles.categorie}>{recipe.tags}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: 400,
        marginVertical: 10,
        display: "flex",
        flexDirection: "column",
        borderWidth: 3,
        borderColor: "#c8c7c7",
        borderRadius: 50,
        alignItems: 'center',
        alignSelf: "center"
    },

    image: {
        borderRadius: 50,
        height: 400,
        width: 400,
        aspectRatio: 1,
    },
    infosContainer: {
        flex: 1,
        marginHorizontal: 50
    },
    ingredientsContainer: {
        display: "flex",
        flexDirection: "row",
        marginVertical: 2,
        paddingLeft: 5
    },
    title: {
        fontSize: 20,
        color: "#1f1f1f",
        marginBottom:10,
        fontWeight: "bold"
    },
    categorie: {
        color: "#1f1f1f",
        marginBottom:10,
        fontSize: 20
    },
    ingredientPlaceholder: {
        fontSize: 15,
        color: "#1f1f1f",
        marginBottom: 3
    },
    imageContainer: {
        width: 400,
        display: "flex",
        justifyContent: "center",
    }
});

export default RecipeCard
