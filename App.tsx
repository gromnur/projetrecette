import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Home from "./src/screens/Home";
import Details from "./src/screens/Details";
import Search from "./src/screens/Search";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {RootStackParamList} from "./src/Objets";

const RootStack = createStackNavigator<RootStackParamList>();

export default function App() {
  return (
      <NavigationContainer>
          <RootStack.Navigator initialRouteName="Home">
              <RootStack.Screen name="Home" component={Home} />
              <RootStack.Screen name="Search" component={Search} />
              <RootStack.Screen name="Details" component={Details} />
          </RootStack.Navigator>
      </NavigationContainer>
  );
}
